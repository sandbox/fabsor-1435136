<?php
class NodeStreamWebTestCase extends DrupalWebTestCase {

  /**
   * This setUp properly works without warnings when using another installation
   * profile than testing.
   */
  protected function setUp() {
    $modules = func_get_args();
    $this->profile = 'nodestream';
    parent::setUp($modules);
  }
}
