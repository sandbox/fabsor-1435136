<?php
/**
 * @file
 * ns_workbench.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function ns_workbench_strongarm() {
  $export = array();

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'ns_workbench_enabled';
  $strongarm->value = '1';
  $export['ns_workbench_enabled'] = $strongarm;

  return $export;
}
