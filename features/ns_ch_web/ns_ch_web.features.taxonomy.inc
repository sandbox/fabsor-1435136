<?php
/**
 * @file
 * ns_ch_web.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function ns_ch_web_taxonomy_default_vocabularies() {
  return array(
    'ns_ch_web_topic' => array(
      'name' => 'Topic',
      'machine_name' => 'ns_ch_web_topic',
      'description' => '',
      'hierarchy' => '0',
      'module' => 'taxonomy',
      'weight' => '0',
    ),
    'ns_ch_web_topic_region' => array(
      'name' => 'Topic region',
      'machine_name' => 'ns_ch_web_topic_region',
      'description' => '',
      'hierarchy' => '0',
      'module' => 'taxonomy',
      'weight' => '0',
    ),
  );
}
