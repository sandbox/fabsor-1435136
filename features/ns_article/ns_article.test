<?php

class NSArticleTest extends NodeStreamWebTestCase {
  public static function getInfo() {
    return array(
      'name' => 'Article tests',
      'description' => 'Test article functionality.',
      'group' => 'NodeStream'
    );
  }

  function setUp() {
    parent::setUp('ns_article');
//    $this->testUser = $this->drupalCreateUser(array('access content'));
//    $this->editor = $this->drupalCreateUser(array('access content', 'create ns_article content', 'edit own ns_article content', 'access content overview'));
  }

  /**
   * Test basic things:
   *  - Is the content type available?
   *  - Do we have all expected fields?
   *  - programatic CRUD.
   */
  function testBasic() {
    $types = node_type_get_types();
    $this->assertTrue(isset($types['ns_article']), t('The article content type exists'));
    // Get all fields and make sure the fields defined are there.
    $fields = array(
      'field_ns_article_body',
      'field_ns_article_lead',
      'field_ns_article_kicker',
      'field_ns_article_related'
    );
    $this->assertFieldsExisting($fields);
    $node = $this->drupalCreateNode($this->getTestNode());
    $saved_node = node_load($node->nid, NULL, TRUE);
    $this->assertTestNode($saved_node);
    // Create a node with a relationship to the first one.
    $node = $this->drupalCreateNode($this->getTestNode($saved_node->nid));
    $saved_second_node = node_load($node->nid, NULL, TRUE);
    $this->assertTestNode($saved_second_node, $saved_node->nid);
  }

  function assertFieldsExisting($fields) {
    $existing_fields = field_info_fields();
    foreach ($fields as $field) {
      $this->assertTrue(isset($existing_fields[$field]), t('The field %field exists.', array('%field' => $field)));
    }
  }

  function testArticleAdmin() {
    $editor = $this->drupalCreateUser(array('access content', 'create ns_article content', 'edit own ns_article content', 'access content overview', 'view own unpublished content'));
    $this->drupalLogin($editor);
    $this->drupalGet('node/add/ns-article');
    $this->assertField('edit-field-ns-article-body-und-0-value', t('The article body field is present.'));
    $this->assertField('edit-field-ns-article-lead-und-0-value', t('The article lead field is present.'));
    $this->assertField('edit-field-ns-article-kicker-und-0-value', t('The article kicker field is present.'));
    $this->assertField('edit-field-ns-article-media-und-0-fid', t('The article media field is present.'));
    $this->assertField('edit-field-ns-article-related-und-0-target-id', t('The related articles field is present.'));
    // The contributor field should not be present at this point.
    $this->assertNoField('edit-field-ns-article-contributor-und-0-target-id', t('The article contributor field is not present.'));
    // We should have some references dialog links.
    $this->assertLink('Create Article');
    $this->assertLink('Search');
    // Post a basic article.
    $edit = array(
      'title' => $this->randomString(),
      'field_ns_article_body[und][0][value]' => $this->loremIpsum(),
      'field_ns_article_lead[und][0][value]' => $this->loremIpsum(),
      'field_ns_article_kicker[und][0][value]' => $this->randomString(),
    );
    $this->drupalPost(NULL, $edit, t('Save'));
  }

  private function getTestNode($related = FALSE) {
    $node = array();
    $node['uid'] = 0;
    $node['type'] = 'ns_article';
    $node['title'] = $this->randomName();
    $node['status'] = 1;
    $node['field_ns_article_body'] = array(
      'und' => array(
        array(
          'value' => $this->loremIpsum(),
        ),
      ),
    );
    $node['field_ns_article_lead'] = array(
      'und' => array(
        array(
          'value' => $this->loremIpsum(),
        ),
      ),
    );
    $node['field_ns_article_kicker'] = array(
      'und' => array(
        array(
          'value' => $this->randomString(),
        ),
      ),
    );
    if ($related) {
      $related_entities = array();
      if (is_array($related)) {
        foreach ($related as $nid) {
          $related_entities[] = array('target_type' => 'node', 'target_id' => $nid);
        }
      }
      else {
        $related_entities[] = array('target_type' => 'node', 'target_id' => $related);
      }
      $node['field_ns_article_related'] = array('und' => $related_entities);
    }
    return $node;
  }

  public function assertTestNode($node, $related = FALSE) {
    $this->assertTrue(isset($node->field_ns_article_body['und'][0]['value']), t('Body exists'));
    $this->assertTrue(isset($node->field_ns_article_lead['und'][0]['value']), t('Lead exists'));
    $this->assertTrue(isset($node->field_ns_article_kicker['und'][0]['value']), t('Kicker exists'));
    if ($related) {
      if (!is_array($related)) {
        $related = array($related);
      }
      $entries = field_get_items('node', $node, 'field_ns_article_related');
      $this->assertEqual(count($entries), count($related));
      foreach ($entries as $key => $entry) {
        $this->assertEqual($entry['target_type'], 'node');
        $this->assertEqual($entry['target_id'], $related[$key]);
      }
    }
  }

  private function loremIpsum() {
    return 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.';
  }
}