<?php
/**
 * @file
 * ns_ch_blog.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function ns_ch_blog_default_rules_configuration() {
  $items = array();
  $items['rules_ns_ch_blog_clear_blog'] = entity_import('rules_config', '{ "rules_ns_ch_blog_clear_blog" : {
      "LABEL" : "Clear blog cache",
      "PLUGIN" : "reaction rule",
      "REQUIRES" : [ "rules", "cache_actions" ],
      "ON" : [ "node_insert", "node_delete", "node_update" ],
      "IF" : [
        { "node_is_of_type" : {
            "node" : [ "node" ],
            "type" : { "value" : { "ns_blog" : "ns_blog", "ns_blog_post" : "ns_blog_post" } }
          }
        }
      ],
      "DO" : [
        { "cache_actions_action_clear_panels_pane_cache" : { "panes" : { "value" : {
                "task:ns_ch_blog_site_template:site_template:new-4" : "task:ns_ch_blog_site_template:site_template:new-4",
                "subtask:page_ns_ch_blog_blog_panel_context:page:ns_ch_blog_blog:new-1" : "subtask:page_ns_ch_blog_blog_panel_context:page:ns_ch_blog_blog:new-1",
                "task:ns_ch_blog_blog:node_view:new-1" : "task:ns_ch_blog_blog:node_view:new-1",
                "task:ns_ch_blog_blog:node_view:new-5" : "task:ns_ch_blog_blog:node_view:new-5",
                "task:ns_ch_blog_blog_post:node_view:new-1" : "task:ns_ch_blog_blog_post:node_view:new-1",
                "task:ns_ch_blog_blog_post:node_view:new-2" : "task:ns_ch_blog_blog_post:node_view:new-2",
                "task:ns_ch_blog_blog_post:node_view:new-9" : "task:ns_ch_blog_blog_post:node_view:new-9"
              }
            }
          }
        }
      ]
    }
  }');
  return $items;
}
