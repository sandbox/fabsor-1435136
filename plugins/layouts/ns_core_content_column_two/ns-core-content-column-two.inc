<?php

$plugin = array(
  'title' => t('Content: two columns'),
  'theme' => 'ns_core_content_column_two',
  'icon' => 'ns-core-content-column-two.png',
  'category' => 'NodeStream',
  'regions' => array(
    'left' => t('Left'),
    'right' => t('Right'),
  ),
);

/**
 * Implementation of theme_preprocess_precision_column_two().
 */
function ns_core_preprocess_ns_core_content_column_two(&$vars) {
  ns_core_check_layout_variables($vars);
}
